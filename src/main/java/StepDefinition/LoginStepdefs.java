package StepDefinition;

import Common.BasePage;
import PageObject.LoginPage;
import PageObject.ProductsPage;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.en.And;
import org.testng.Assert;

public class LoginStepdefs extends BasePage {

    LoginPage loginPage;
    {
        loginPage = new LoginPage(wDriver);
    }
    ProductsPage productsPage =new ProductsPage(wDriver);
    @And("I land on Swaglabs Login page$")
    public void iLandOnSwaglabsLoginPage() {
        loginPage = new LoginPage(wDriver);
        Assert.assertTrue(loginPage.verifyLoginPage(), "Unable to verify login page");
    }

    @When("I enter username \"([^\"]*)\" and password \"([^\"]*)\"$")
    public void iEnterUsernameAndPassword(String username, String password) {
        loginPage = new LoginPage(wDriver);
        Assert.assertTrue(loginPage.captureUsername(username), "Unable to capture username");
        Assert.assertTrue(loginPage.capturePassword(password), "Unable to capture password");
    }

    @And("I click login button$")
    public void iClickLoginButton() {
        Assert.assertTrue(loginPage.clickLoginButton(), "Unable to click login button");
    }

    @Then("Verify that i can login successfully$")
    public void verifyThatICanLoginSuccessfully() {
        Assert.assertTrue(productsPage.verifyProductPage(), "Unable to login: Login was unsuccessful");
    }

    @Then("Verify that i can not login successfully \"([^\"]*)\"$")
    public void verifyThatICanNotLoginSuccessfully(String loginMessage) {
        Assert.assertTrue(loginPage.verifyLoginMessage(loginMessage), "Unable verify login error message");
    }
}

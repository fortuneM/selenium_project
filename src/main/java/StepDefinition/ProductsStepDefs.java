package StepDefinition;

import Common.BasePage;
import PageObject.CartPage;
import PageObject.CheckoutPage;
import PageObject.ProductsPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.en.And;
import org.testng.Assert;

public class ProductsStepDefs extends BasePage {

    ProductsPage productsPage =new ProductsPage(wDriver);
    CartPage cartPage = new CartPage(wDriver);
    CheckoutPage checkoutPage = new CheckoutPage(wDriver);

    @And("I add a product \"([^\"]*)\" to cart$")
    public void iClickAddToCart(String product) {
        Assert.assertTrue(productsPage.clickAddToCart(product), "Unable to click add to cart button");
    }

    @And("I click checkout$")
    public void iClickCheckout() {
        Assert.assertTrue(cartPage.clickCheckoutButton(), "Unable to click add to cart button");
    }

    @And("I enter checkout details \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
    public void iEnterCheckoutDetails(String fname, String lname, String postcode) {
        Assert.assertTrue(checkoutPage.captureFirstname(fname), "Unable to capture firstname");
        Assert.assertTrue(checkoutPage.captureLastname(lname), "Unable to capture lastname");
        Assert.assertTrue(checkoutPage.capturepostalCode(postcode), "Unable to capture postal code");
    }
    @And("I click continue$")
    public void iClickContinueBtn() {
        Assert.assertTrue(checkoutPage.clickContinueButton(), "Unable to click Continue button");
    }

    @And("I click finish$")
    public void iClickFinishBtn() {
        Assert.assertTrue(checkoutPage.clickFinishButton(), "Unable to click Continue button");
    }

    @And("I can verify that order is completed$")
    public void iVerifyOrderIsComplete() {
        Assert.assertTrue(checkoutPage.verifyOrderIsComplete(), "Unable to click Continue button");
    }

    @And("I click on cart$")
    public void iClickOnCart() {
        Assert.assertTrue(productsPage.clickCartButton(), "Unable to click cart button");
    }
}

package StepDefinition;

import Common.BasePage;
import cucumber.api.java.en.Given;

public class CommonStepdefs extends BasePage {

    @Given("As a user i launch Swaglabs application$")
    public void asAUserILaunchSwaglabsApplication() {
        System.out.println("Opening Browser and launching application...");
        openBrowser();
    }

}

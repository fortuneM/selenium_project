package PageObject;

import Common.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class CartPage extends BasePage {

    @FindBy(id = "checkout")
    private WebElement btnCheckout;

    public CartPage(WebDriver wDriver) {
        super();
        PageFactory.initElements(wDriver, this);
    }

    /**
     * Description: This method clicks checkout button
     */
    public boolean clickCheckoutButton() {
        wDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        if (btnCheckout.isDisplayed()) {
            btnCheckout.click();
            return true;
        }
        return false;
    }
}

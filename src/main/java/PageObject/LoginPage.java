package PageObject;


import Common.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class LoginPage extends BasePage {

    @FindBy(id = "user-name")
    private WebElement inputUsrname;

    @FindBy(id = "password")
    private WebElement inputPassword;

    @FindBy(id = "login-button")
    private WebElement btnLogin;

    @FindBy(className = "login_logo")
    private WebElement lblLoginlogo;

    @FindBy(xpath = "//*[text()='Epic sadface: Sorry, this user has been locked out.']")
    private WebElement lblLoginErrorMessage;

    public LoginPage(WebDriver wDriver) {
        super();
        PageFactory.initElements(wDriver,this);
    }


    /**
     * Description: This method captures login username
     */
    public boolean captureUsername(String usrname) {
        wDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        if (inputUsrname.isDisplayed()) {
            inputUsrname.sendKeys(usrname);
            return true;
        }
        return false;
    }

    /**
     * Description: This method captures login password
     */
    public boolean capturePassword(String password) {
        wDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        if (inputPassword.isDisplayed()) {
            inputPassword.sendKeys(password);
            return true;
        }
        return false;
    }

    /**
     * Description: This method click login button
     */
    public boolean clickLoginButton() {
        wDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        if (btnLogin.isDisplayed()) {
            btnLogin.click();
            return true;
        }
        return false;
    }

    /**
     * Description: This method verifies Login landing page
     */
    public boolean verifyLoginPage() {
        wDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        return lblLoginlogo.isDisplayed();
    }

    /**
     * Description: This method verifies Login error messages
     */
    public boolean verifyLoginMessage(String expectedMessage) {
        wDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        return lblLoginErrorMessage.getText().equalsIgnoreCase(expectedMessage);
    }


}

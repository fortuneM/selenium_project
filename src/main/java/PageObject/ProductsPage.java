package PageObject;

import Common.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class ProductsPage extends BasePage {

    @FindBy(className = "title")
    private WebElement lblProductsTitle;

    @FindBy(id = "add-to-cart-sauce-labs-backpack")
    private WebElement btnAddBackPack;

    @FindBy(id = "add-to-cart-sauce-labs-bolt-t-shirt")
    private WebElement btnAddTshirt;

    @FindBy(id = "add-to-cart-sauce-labs-bike-light")
    private WebElement btnAddBikeLight;

    @FindBy(id = "shopping_cart_container")
    private WebElement btnCart;

    public ProductsPage(WebDriver wDriver) {
        super();
        PageFactory.initElements(wDriver, this);
    }

    /**
     * Description: This method verifies that Products Page is displayed
     */
    public boolean verifyProductPage() {
        wDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        return lblProductsTitle.isDisplayed();
    }

    /**
     * Description: This method click Cart button
     */
    public boolean clickCartButton() {
        wDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        if (btnCart.isDisplayed()) {
            btnCart.click();
            return true;
        }
        return false;
    }

    /**
     * Description: This method clicks add to cart button
     */
    public boolean clickAddToCart(String product) {
        if (product.equalsIgnoreCase("backpack")) {
            if (btnAddBackPack.isDisplayed()) {
                btnAddBackPack.click();
                return true;
            }
        } else if (product.equalsIgnoreCase("tshirt") && btnAddTshirt.isDisplayed()) {
            btnAddTshirt.click();
            return true;
        } else if (product.equalsIgnoreCase("bikeLight") && btnAddBikeLight.isDisplayed()) {
            btnAddBikeLight.click();
            return true;
        }
        return false;
    }

}

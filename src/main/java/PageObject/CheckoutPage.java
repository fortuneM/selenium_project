package PageObject;

import Common.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class CheckoutPage extends BasePage {

    @FindBy(id = "first-name")
    private WebElement inputFirstname;

    @FindBy(id = "last-name")
    private WebElement inputLastname;

    @FindBy(id = "postal-code")
    private WebElement inputPostalCode;

    @FindBy(id = "continue")
    private WebElement btnContinue;

    @FindBy(id = "finish")
    private WebElement btnFinish;

    @FindBy(xpath = "//*[text()='Checkout: Complete!']")
    private WebElement lblComplete;

    @FindBy(xpath = "//*[text()='THANK YOU FOR YOUR ORDER']")
    private WebElement lblThankYou;

    public CheckoutPage(WebDriver wDriver) {
        super();
        PageFactory.initElements(wDriver, this);

    }

    /**
     * Description: This method captures firstname
     */
    public boolean captureFirstname(String firstname) {
        wDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        if (inputFirstname.isDisplayed()) {
            inputFirstname.sendKeys(firstname);
            return true;
        }
        return false;
    }

    /**
     * Description: This method captures lastname
     */
    public boolean captureLastname(String lastname) {
        wDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        if (inputLastname.isDisplayed()) {
            inputLastname.sendKeys(lastname);
            return true;
        }
        return false;
    }

    /**
     * Description: This method captures postal code
     */
    public boolean capturepostalCode(String postcode) {
        wDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        if (inputPostalCode.isDisplayed()) {
            inputPostalCode.sendKeys(postcode);
            return true;
        }
        return false;
    }

    /**
     * Description: This method click Continue button
     */
    public boolean clickContinueButton() {
        wDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        if (btnContinue.isDisplayed()) {
            btnContinue.click();
            return true;
        }
        return false;
    }

    /**
     * Description: This method click Finish button
     */
    public boolean clickFinishButton() {
        wDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        if (btnFinish.isDisplayed()) {
            btnFinish.click();
            return true;
        }
        return false;
    }

    /**
     * Description: This method verifies that the order is completed successful
     */
    public boolean verifyOrderIsComplete() {
        if (lblComplete.isDisplayed() && lblThankYou.isDisplayed()) {
            return true;
        }
        return false;
    }


}

package Common;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.PageFactory;

public class BasePage {

    public static WebDriver wDriver;

    public void openBrowser() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/webdrivers/chromedriver.exe");
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("useAutomationExtension", false);
        wDriver = new ChromeDriver();
        wDriver.manage().window().maximize();
        wDriver.get("https://www.saucedemo.com/");
    }


}

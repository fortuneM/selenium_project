
@RunRegression
@PurchaseProducts
Feature: Test Login functionality and Navigation on Swaglabs application

  @Purchase
  Scenario Outline: <scenarioname>
    Given As a user i launch Swaglabs application
    And I land on Swaglabs Login page
    When I enter username "<username>" and password "<password>"
    And I click login button
    And Verify that i can login successfully
    And I add a product "<productname>" to cart
    And I click on cart
    And I click checkout
    And I enter checkout details "<firstname>" "<lastname>" "<postcode>"
    And I click continue
    And I click finish
    Then I can verify that order is completed

    Examples:
      | scenarioname                  | username      | password     | productname | firstname | lastname | postcode |
      | order a backpack and checkout | standard_user | secret_sauce | Backpack    | Fortune   | Mlalazi  | 0122     |
      | order a tshirt and checkout   | standard_user | secret_sauce | tshirt      | Fortune   | Mlalazi  | 0123     |
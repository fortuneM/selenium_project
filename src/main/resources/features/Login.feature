@RunRegression
@Login
Feature: Test Login functionality and Navigation on Swaglabs application

  @LoginPositiveTest
  Scenario Outline: <scenarioname>
    Given As a user i launch Swaglabs application
    And I land on Swaglabs Login page
    When I enter username "<username>" and password "<password>"
    And I click login button
    Then Verify that i can login successfully

    Examples:
      | scenarioname                              | username      | password     |
      | Login to swaglabs site with standard_user | standard_user | secret_sauce |

  @LoginNegativeTest
  Scenario Outline: <scenarioname>
    Given As a user i launch Swaglabs application
    And I land on Swaglabs Login page
    When I enter username "<username>" and password "<password>"
    And I click login button
    Then Verify that i can not login successfully "<loginmessage>"

    Examples:
      | scenarioname                                        | username        | password     | loginmessage                                        |
      | Login to swaglabs site with locked_out_user         | locked_out_user | secret_sauce | Epic sadface: Sorry, this user has been locked out. |
#      | Login to swaglabs site with problem_user            | problem_user    | secret_sauce |                                                     |
#      | Login to swaglabs site with performance_glitch_user | standard_user   | secret_sauce |                                                     |
